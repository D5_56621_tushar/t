package com.sunbeam.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sunbeam.entities.Post;
import com.sunbeam.entities.User;

public interface PostDao extends JpaRepository<Post, Integer> {
	
	@Query(value = "SELECT * FROM post WHERE userid=?1", nativeQuery = true)
	List<Post> findByUserId(@Param("userId") int id);

	@Query(value = "SELECT * FROM post WHERE isreported!=0 ORDER BY isreported DESC", nativeQuery = true)
	List<Post> findAllReported();
	
	@Query(value = "SELECT * FROM post", nativeQuery = true)
	List<Post> findAllPosts();
	
	@Modifying
	@Query(value = "INSERT INTO post (userId, postDetails, tags) VALUES (?1, ?2, ?3)", nativeQuery = true)
	int addPost(int userId, String postDetails, String tags);
	
}
