package com.sunbeam.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.dtos.PostAttachmentFormDTO;
import com.sunbeam.dtos.Response;
import com.sunbeam.entities.Post;
import com.sunbeam.entities.PostAttachment;
import com.sunbeam.services.PostServiceImpl;
import com.sunbeam.services.UserServiceImpl;

@CrossOrigin
@RestController
public class PostControllerImpl {

	@Autowired
	private PostServiceImpl postService;
	
	@Autowired
	private UserServiceImpl userService;
	
	@GetMapping("/posts/{id}")
	public ResponseEntity<?> findByUserId(@PathVariable("id") int userId) {
		List<Post> posts = postService.findByUserId(userId);
		if(posts.size() == 0)
			return Response.error("No posts found!!!");  
		return Response.success(posts);   
	}
	
	@GetMapping("/posts")
	public ResponseEntity<?> findAllPosts() {
		List<Post> posts = postService.findAllPosts();
		if(posts.size()==0)
			return Response.error("No posts found!!!");
		return Response.success(posts);    
	}
	
	@PostMapping("/post")
	public ResponseEntity<?> addPost(@RequestBody Post post) {
		System.out.println(post);
		int count = postService.addPost(post);
		if(count == 0)
			return Response.error("Some problem occurred!!!");
		return Response.success("Post added successfully!!!");
	}
	
	@GetMapping("/posts/reported")
	public ResponseEntity<?> findAllReportedPosts() {
		List<Post> reportedPosts = postService.findAllReported();
		if(reportedPosts.size()==0) {
			System.out.println("dfgsvdfgd");
			return Response.error("No reported posts found!!!");
		}
		return Response.success(reportedPosts); 
	}
	
	@DeleteMapping("/posts/{id}")
	public ResponseEntity<?> deletePostById(@PathVariable("id") int postId) {
		int count = postService.deletePostById(postId);
		if(count==0)
			return Response.error("Post not found!!!");
		return Response.success("Post deleted successfully!!!"); 
	}
	
	@PostMapping("/attachment/{postId}")
	public ResponseEntity<?> addAttachment(@PathVariable int postId, PostAttachmentFormDTO attachment) {
		PostAttachment att = postService.addPostAttachment(postId, attachment);
		if(att == null)
			return Response.error("Some Error Occurred!!!");
		return Response.success("Attachment added successfully!!!");
	}
	
}
