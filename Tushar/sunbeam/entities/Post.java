package com.sunbeam.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "post")
public class Post {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="userId")
	private User user;
	
	private String postDetails;
	private String tags;
	private int state;
	
	@OneToMany(mappedBy = "post")
	private List<PostAttachment> attachList;
	@OneToMany(mappedBy = "post")
	private List<PostLikeStatus> likesList;
	@OneToMany(mappedBy = "post")
	private List<PostComment> commentList;
	
//	@Column(columnDefinition = "Default 0")
	@Column(insertable = false)
	private Integer isReported;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(insertable = false)
	private Date createdTimestamp;
	
	
	
	public Post() {
		super();
	}

	public Post(Integer id, User user, String postDetails, String tags, int state, Integer isReported,
			Date createdTimestamp) {
		super();
		this.id = id;
		this.user = user;
		this.postDetails = postDetails;
		this.tags = tags;
		this.state = state;
		this.isReported = isReported;
		this.createdTimestamp = createdTimestamp;
	}

	public Post(int id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getPostDetails() {
		return postDetails;
	}

	public void setPostDetails(String postDetails) {
		this.postDetails = postDetails;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Integer getIsReported() {
		return isReported;
	}

	public void setIsReported(Integer isReported) {
		this.isReported = isReported;
	}

	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	@Override
	public String toString() {
		return "Post [id=" + id + ", user=" + user + ", postDetails=" + postDetails + ", tags=" + tags + ", state="
				+ state + ", isReported=" + isReported + ", createdTimestamp=" + createdTimestamp + "]";
	}
	
	
	
}
