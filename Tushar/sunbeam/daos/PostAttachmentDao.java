package com.sunbeam.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sunbeam.entities.PostAttachment;

public interface PostAttachmentDao extends JpaRepository<PostAttachment, Integer> {

	
}
