package com.sunbeam.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.sunbeam.entities.User;

public interface UserDao extends JpaRepository<User, Integer> {
	
	@Query(value = "SELECT * FROM user WHERE state = 'user'", nativeQuery = true)
	List<User> findAllNonAdmins();
	
	User findById(int id);
	User findByEmail(String email);

	@Modifying
	@Query(value = "UPDATE user SET isActive=0 WHERE id = ?1", nativeQuery = true)
	int blockUser(int userId);

	@Modifying
	@Query(value = "INSERT INTO user (firstName, lastName, email, password) VALUES (?1, ?2, ?3, ?4)", nativeQuery = true)
	int saveUser(String firstName, String lastName, String email, String password);
}

