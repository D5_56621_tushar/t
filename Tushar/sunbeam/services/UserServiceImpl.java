package com.sunbeam.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sunbeam.daos.UserDao;
import com.sunbeam.dtos.Credentials;
import com.sunbeam.dtos.DTOEntityConvertor;
import com.sunbeam.entities.User;

@Transactional
@Service
public class UserServiceImpl {
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private DTOEntityConvertor convertor;

	public List<User> findAllNonAdmins() {
		return userDao.findAllNonAdmins();
	}
	
	public List<User> findAll() {
		return userDao.findAll();
	}
	
	public User findUserById(int id) {
		return userDao.findById(id);
	}
	
	public User findUserByEmail(String email) {
		return userDao.findByEmail(email);
	}
	
	public User saveUser(User user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
//		return userDao.saveUser(user.getFirstName(), user.getLastName(), user.getEmail(), user.getPassword());
		return userDao.save(user);
	}
	
	public User findUserByEmailAndPassword(Credentials cred) {
		User user = userDao.findByEmail(cred.getEmail());
		if(user!=null && passwordEncoder.matches(cred.getPassword(), user.getPassword()))
			return user;
		return null;
	}

	public int blockUser(int userId) {
		User user = userDao.findById(userId);
		if(user.getIsActive() == 0)
			return 0;
		userDao.blockUser(userId);
		return 1;
	}
}

/*
 * return Collections.singletonMap("insertedId", user.getId());
 *
 * OR
 * 
 * HashMap<String, Object> map = new HashMap<>();
 * map.put("insertedId", user.getId())
 * return map;
 * 
 * */



