package com.sunbeam.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sunbeam.daos.PostAttachmentDao;
import com.sunbeam.daos.PostDao;
import com.sunbeam.dtos.DTOEntityConvertor;
import com.sunbeam.dtos.PostAttachmentFormDTO;
import com.sunbeam.entities.Post;
import com.sunbeam.entities.PostAttachment;

@Transactional
@Service
public class PostServiceImpl {

	@Autowired
	private PostDao postDao;
	
	@Autowired
	private PostAttachmentDao attachmentDao;
	
	@Autowired
	private DTOEntityConvertor convertor;
	
	public List<Post> findByUserId(int userId) {
		return postDao.findByUserId(userId);
	}

	public List<Post> findAllPosts() {
		return postDao.findAllPosts();
	}

	public List<Post> findAllReported() {
		return postDao.findAllReported();
	}

	public int deletePostById(int postId) {
		Optional<Post> posts = postDao.findById(postId);
		if(posts.isEmpty())
			return 0;
		postDao.deleteById(postId);
		return 1;
	}

	public int addPost(Post post) {
		return postDao.addPost(post.getUser().getId(), post.getPostDetails(), post.getTags());
	}

	public PostAttachment addPostAttachment(int postId, PostAttachmentFormDTO attachmentDTO) {
		attachmentDTO.setPostId(postId);
		PostAttachment attachment = convertor.toAttachmentEntity(attachmentDTO);
		attachment = attachmentDao.save(attachment);
		return attachment;
	}

}
