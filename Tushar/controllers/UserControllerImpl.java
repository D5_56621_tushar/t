package com.sunbeam.controllers;

import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.dtos.Credentials;
import com.sunbeam.dtos.Response;
import com.sunbeam.entities.Post;
import com.sunbeam.entities.User;
import com.sunbeam.services.PostServiceImpl;
import com.sunbeam.services.UserServiceImpl;

@CrossOrigin
@RestController
@RequestMapping("/users")
public class UserControllerImpl {

	@Autowired
	private UserServiceImpl userService;
	
	@Autowired
	private PostServiceImpl postService;
	
	@GetMapping("")
	public ResponseEntity<?> findAllNonAdmins() {
		List<User> users = userService.findAllNonAdmins();
		if(users.size() == 0)
			return Response.status(HttpStatus.NOT_FOUND);
		return Response.success(users);
	}
	
	@GetMapping("/all")
	public ResponseEntity<?> findAll() {
		List<User> users = userService.findAll();
		if(users.size() == 0) {
			System.out.println("sfefjiujhf");
			return Response.status(HttpStatus.NOT_FOUND);
		}
		return Response.success(users);
	}
	
	@PostMapping("/signup")
	public ResponseEntity<?> signup(@RequestBody User user) {
//		User u = userService.findUserByEmail(user.getEmail());
//		if(u!=null)
//			return Response.error("User with this email address is already present!!!");
		userService.saveUser(user);
		return Response.success("Signup successfull!!!");
	}
	
	@PatchMapping("/block/{id}")
	public ResponseEntity<?> blockUser(@PathVariable("id") int userId) {
		int count = userService.blockUser(userId);
		if(count == 0)
			return Response.error("User is already Blocked!!!");
		return Response.success("User Blocked successfully!!!");
	}
	
	@PostMapping("/signin")
	public ResponseEntity<?> signIn(@RequestBody Credentials cred) {
		User user = userService.findUserByEmailAndPassword(cred);
		if(user == null)
			return Response.error("Invalid username or password");
		return Response.success(user);
	}
	
	@ExceptionHandler(DataIntegrityViolationException.class)
	public ResponseEntity<?> emailExistsHandler(DataIntegrityViolationException ex) {
		return Response.error("User with this email address is already present!!!");
	}
}
