package com.sunbeam.dtos;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class PostAttachmentFormDTO {

	int id;
	private int postId;

	private MultipartFile dataFile;
	
	private Date createdTimeStamp;
	
	public PostAttachmentFormDTO() {
		super();
	}

	public PostAttachmentFormDTO(int id, Date createdTimeStamp, MultipartFile dataFile, int postId) {
		super();
		this.id = id;
		this.createdTimeStamp = createdTimeStamp;
		this.dataFile = dataFile;
		this.postId = postId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedTimeStamp() {
		return createdTimeStamp;
	}

	public void setCreatedTimeStamp(Date createdTimeStamp) {
		this.createdTimeStamp = createdTimeStamp;
	}

	public MultipartFile getDataFile() {
		return dataFile;
	}

	public void setDataFile(MultipartFile dataFile) {
		this.dataFile = dataFile;
	}

	public int getPostId() {
		return postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	@Override
	public String toString() {
		return "PostAttachmentFormData [id=" + id + ", createdTimeStamp=" + createdTimeStamp + ", dataFile=" + dataFile
				+ ", postId=" + postId + "]";
	}

	
}
