package com.sunbeam.dtos;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartException;

import com.sunbeam.entities.Post;
import com.sunbeam.entities.PostAttachment;
import com.sunbeam.entities.User;

@Component
public class DTOEntityConvertor {

	public UserDTO toUserDTO(User user) {
		UserDTO userDTO = new UserDTO();
		userDTO.setId(user.getId());
		userDTO.setFirstName(user.getFirstName());
		userDTO.setLastName(user.getLastName());
		userDTO.setEmail(user.getEmail());
		return userDTO;
	}

	public PostAttachment toAttachmentEntity(PostAttachmentFormDTO attachmentDTO) {
		
		if(attachmentDTO == null)
			return null;
		PostAttachment entity = new PostAttachment();
		entity.setId(attachmentDTO.getId());
		entity.setCreatedTimestamp(attachmentDTO.getCreatedTimeStamp());
		entity.setPost(new Post(attachmentDTO.getPostId()));
		try {
			entity.setData(attachmentDTO.getDataFile().getBytes());
		} catch (Exception e) {
			throw new MultipartException("Can't convert multipart file to bytes : " + attachmentDTO.getDataFile(), e);
		}
		return entity;
	}
}
