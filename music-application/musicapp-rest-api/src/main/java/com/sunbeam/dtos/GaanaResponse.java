package com.sunbeam.dtos;

import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value = Include.NON_NULL)
public class GaanaResponse {
	public static enum Status {
		success, fail, error
	}
	
	private Status status;
	private Object data;
	private String message;
	
	public GaanaResponse(Status status, Object data) {
		this.status = status;
		this.data = data;
	}

	public GaanaResponse(Status status, String message) {
		this.status = status;
		this.message = message;
	}

	public GaanaResponse(Status status, Object data, String message) {
		this.status = status;
		this.data = data;
		this.message = message;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "GaanaResponse [status=" + status + ", data=" + data + ", message=" + message + "]";
	}
	
	public static ResponseEntity<GaanaResponse> success() {
		return ResponseEntity.ok(new GaanaResponse(Status.success, null));
	}
	
	public static ResponseEntity<GaanaResponse> success(Object data) {
		return ResponseEntity.ok(new GaanaResponse(Status.success, data));
	}
	
	public static ResponseEntity<GaanaResponse> success(String message, Object data) {
		return ResponseEntity.ok(new GaanaResponse(Status.success, data, message));
	}
	
	public static ResponseEntity<GaanaResponse> fail(Object data) {
		return ResponseEntity.ok(new GaanaResponse(Status.fail, data));
	}

	public static ResponseEntity<GaanaResponse> fail(String message, Object data) {
		return ResponseEntity.ok(new GaanaResponse(Status.fail, data, message));
	}
	
	public static ResponseEntity<GaanaResponse> error(String message) {
		return ResponseEntity.ok(new GaanaResponse(Status.error, message));
	}

	public static ResponseEntity<GaanaResponse> error(String message, Object data) {
		return ResponseEntity.ok(new GaanaResponse(Status.error, data, message));
	}
}
