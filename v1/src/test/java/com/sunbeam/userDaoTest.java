package com.sunbeam;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.sunbeam.daos.UserDao;

@SpringBootTest
class userDaoTest {

	@Autowired
	private UserDao userDao;
	
	@Test
	void findAllUsers() {
		userDao.findAll().forEach(System.out::println);
	}

}
