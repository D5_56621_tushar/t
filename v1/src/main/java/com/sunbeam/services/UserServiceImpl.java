package com.sunbeam.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sunbeam.daos.UserDao;
import com.sunbeam.dtos.Credentials;
import com.sunbeam.entities.User;

@Transactional
@Service
public class UserServiceImpl {
	@Autowired
	private UserDao userDao;

	public List<User> findAllUsers() {
		return userDao.findAll();
	}
	
	public User findUserById(int id) {
		return userDao.findById(id);
	}
	
	public User findUserByEmail(String email) {
		return userDao.findByEmail(email);
	}
	
	public User saveUser(User user) {
		return userDao.save(user);
	}
	
	public User findUserByEmailAndPassword(Credentials cred) {
	
		User user = userDao.findByEmail(cred.getEmail());
		if(user!=null && user.getPassword().equals(cred.getPassword())) {
			
			return user;
		}
		return null;
	}
}

/*
 * return Collections.singletonMap("insertedId", user.getId());
 *
 * OR
 * 
 * HashMap<String, Object> map = new HashMap<>();
 * map.put("insertedId", user.getId())
 * return map;
 * 
 * */



