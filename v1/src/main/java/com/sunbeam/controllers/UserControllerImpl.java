package com.sunbeam.controllers;

import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.dtos.Credentials;
import com.sunbeam.entities.User;
import com.sunbeam.services.UserServiceImpl;
@CrossOrigin
@RestController
@RequestMapping("/users")
public class UserControllerImpl {

	@Autowired
	private UserServiceImpl userService;
	
	@GetMapping("")
	public @ResponseBody List<User> findAllUsers() {
		return userService.findAllUsers();
	}
	
	@PostMapping("")
	public User saveUser(@RequestBody User user) {
		return userService.saveUser(user);
	}
	
	@PostMapping("/signin")
	public Object signIn(@RequestBody Credentials cred) {
		User user = userService.findUserByEmailAndPassword(cred);
		if(user == null)
			return "user not found";
		return user;
	}
	
	
}
