//insertRecursively
//insertIteratively
//preOrderTraversal
//inOrderTraversal
//postOrderTraversal
//deleteNode
//levelOrderLevelWise
//topViewOfBinaryTree
//bottomViewOfBinaryTree
//heightOfTree
//isBST
//isBalanced (similar to isBST())
//lengthOfLongestPathBetweenAnyTwoNodes (also called diameter of the tree)
//leftChildrenSum (Samsung 2022)

package tree;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.lang.Math;

public class BinarySearchTree {

	private Node root;

	public BinarySearchTree() {
		this.root = null;
	}

	public void insertRecursively(Node temp, int data) {
		if (root == null) {
			root = new Node(data);
			return;
		}

		if (data < temp.data) {
			if (temp.left == null)
				temp.left = new Node(data);
			else
				insertRecursively(temp.left, data);
		} else {
			if (temp.right == null)
				temp.right = new Node(data);
			else
				insertRecursively(temp.right, data);
		}
	}

	public void insertRecursively(int data) {
		insertRecursively(root, data);
	}

	public Node insertRecursively2(Node r, int data) {
		if (r == null)
			return new Node(data);
		if (data < r.data)
			r.left = insertRecursively2(r.left, data);
		else
			r.right = insertRecursively2(r.right, data);
		return r;
	}

	public void insertRecursively2(int data) {
		this.root = insertRecursively2(this.root, data);
	}

	public void insertIteratively(int data) {
		Node newNode = new Node(data);
		if (root == null)
			root = newNode;
		else {
			Node temp = root;

			while (true) {
				if (data < temp.data) {
					if (temp.left == null) {
						temp.left = newNode;
						break;
					} else
						temp = temp.left;
				} else {
					if (temp.right == null) {
						temp.right = newNode;
						break;
					} else
						temp = temp.right;
				}
			}
		}
	}

	public void preOrderTraversal(Node r) {
		if (r == null)
			return;

		System.out.print(r.data + " ");
		preOrderTraversal(r.left);
		preOrderTraversal(r.right);
	}

	public void preOrderTraversal() {
		if (root == null)
			return;
		preOrderTraversal(root);
	}

	public void inOrderTraversal(Node r) {
		if (r == null)
			return;

		inOrderTraversal(r.left);
		System.out.print(r.data + " ");
		inOrderTraversal(r.right);
	}

	public void inOrderTraversal() {
		if (root == null)
			return;
		inOrderTraversal(root);
	}

	public void postOrderTraversal(Node r) {
		if (r == null)
			return;

		postOrderTraversal(r.left);
		postOrderTraversal(r.right);
		System.out.print(r.data + " ");
	}

	public void postOrderTraversal() {
		if (root == null)
			return;
		postOrderTraversal(root);
	}

	public void BFSTraversal() {
		if (root == null)
			return;

		Queue<Node> q = new LinkedList<Node>();
		q.add(root);
		Node temp = null;

		while (!q.isEmpty()) {
			temp = q.poll();
			System.out.print(temp.data + " ");
			if (temp.left != null)
				q.add(temp.left);
			if (temp.right != null)
				q.add(temp.right);
		}
	}

	public Node deleteNode(Node r, int data) {
		if (r == null)
			return null;
		if (data < r.data) {
			r.left = deleteNode(r.left, data);
			return r;
		} else if (data > r.data) {
			r.right = deleteNode(r.right, data);
			return r;
		}
		if (r.left == null && r.right == null)
			return null;
		if (r.left == null && r.right != null)
			return r.right;
		if (r.left != null && r.right == null)
			return r.left;
		Node nextBig = r.right;
		while (nextBig.left != null)
			nextBig = nextBig.left;
		r.data = nextBig.data;
		r.right = deleteNode(r.right, nextBig.data);
		return r;
	}

	public void deleteNode(int data) {
		this.root = deleteNode(this.root, data);
	}
	
	public void levelOrderLevelWise() {
		Queue<Node> q = new LinkedList<Node>();
		q.add(root);
		q.add(null);
		Node temp = null;
		
		while(!q.isEmpty()) {
			temp = q.remove();
			if(temp == null) {
				System.out.println();
				if(!q.isEmpty())
					q.add(null);
				continue;
			}
			if(temp.left!=null)
				q.add(temp.left);
			if(temp.right!=null)
				q.add(temp.right);
			System.out.print(temp.data + " ");
		}
	}
	
	static int min, max;
	
	
	public void topViewOfBinaryTree() {
		if(root == null)
			return;
		min = 0;
		max = 0;
		HashMap<Integer, Node> map = new HashMap<Integer, Node>();
		topViewOfBinaryTree(root, 0, map);
		
		for(int j = min; j<=max; j++) 
			System.out.println(map.get(j).data);
	}
	
	public void topViewOfBinaryTree(Node r, Integer i, HashMap<Integer, Node> map) {
		if(r == null)
			return;
		
		if(i<min)
			min = i;
		if(i>max)
			max = i;
		
		if(!map.containsKey(i))
			map.put(i, r);
		topViewOfBinaryTree(r.left, i-1, map);
		topViewOfBinaryTree(r.right, i+1, map);
	}
	
	public void bottomViewOfBinaryTree() {
		if(root == null)
			return;
		min = 0;
		max = 0;
		HashMap<Integer, Node> map = new HashMap<Integer, Node>();
		bottomViewOfBinaryTree(root, 0, map);
		
		for(int j = min; j<=max; j++) 
			System.out.println(map.get(j).data);
	}
	
	public void bottomViewOfBinaryTree(Node r, Integer i, HashMap<Integer, Node> map) {
		if(r == null)
			return;
		
		if(i<min)
			min = i;
		if(i>max)
			max = i;
		
		map.put(i, r);
		bottomViewOfBinaryTree(r.left, i-1, map);
		bottomViewOfBinaryTree(r.right, i+1, map);
	}
	
	public void lengthOfLongestPathBetweenAnyTwoNodes() {
		System.out.println(lengthOfLongestPathBetweenAnyTwoNodes(this.root));
	}
	
	public int heightOfTree(Node r) {
		if(r == null)
			return -1;
		
		int leftHeight = heightOfTree(r.left);
		int rightHeight = heightOfTree(r.right);

		int height = 1+Math.max(leftHeight, rightHeight);
		
		System.out.println(r.data + ": " + height);
		return height;
	}
	
	public void heightOfTree() {
		System.out.println(heightOfTree(this.root));
	}
	
	public HelperForIsBST isBST(Node r) {
		if(r == null)
			return new HelperForIsBST(true, Integer.MAX_VALUE, Integer.MIN_VALUE);
		
		HelperForIsBST isLeftBST = isBST(r.left);
		HelperForIsBST isRightBST = isBST(r.right);
		Integer minLeft = isLeftBST.minOfTree;
		Integer maxLeft = isLeftBST.maxOfTree;
		Integer minRight = isRightBST.minOfTree;
		Integer maxRight = isRightBST.maxOfTree;
		Boolean isL = isLeftBST.isBST;
		Boolean isR = isRightBST.isBST;
		

		System.out.println(r.data + ": " + (isL && isR && maxLeft<=r.data && minRight>r.data) + ", " + Math.min(minLeft, r.data)+ "," + Math.max(maxRight, r.data));
		
		return new HelperForIsBST(isL && isR && maxLeft<=r.data && minRight>r.data, Math.min(minLeft, r.data), Math.max(maxRight, r.data));
	}
	
	public void isBST() {
		System.out.println(isBST(this.root));
	}
	
	public HelperForIsBalanced isBalanced(Node r) {
		if(r == null)
			return new HelperForIsBalanced(true, -1);
		
		HelperForIsBalanced leftIsBalanced = isBalanced(r.left);
		HelperForIsBalanced rightIsBalanced = isBalanced(r.right);
		
		boolean isBalanced;	
		
		if(r.left!=null && r.right!=null) {
			System.out.print("if=> ");
			isBalanced = leftIsBalanced.isBalanced && rightIsBalanced.isBalanced && Math.abs(leftIsBalanced.height - rightIsBalanced.height) <= 1;
		}
		else {
			System.out.print("else=> ");
			isBalanced = leftIsBalanced.isBalanced && rightIsBalanced.isBalanced && Math.abs(leftIsBalanced.height - rightIsBalanced.height) <= 0;
		}
		
		int height = Math.max(leftIsBalanced.height, rightIsBalanced.height) + 1;
//		if(r.left!=null || r.right!=null)
//			height++;
		
		System.out.println(r.data + ": " + isBalanced+ "," + height);
		return new HelperForIsBalanced(isBalanced, height);
	}
	
	public void isBalanced() {
		System.out.println(isBalanced(this.root));
	}
	
	public HelperForlengthOfLongestPath lengthOfLongestPathBetweenAnyTwoNodes (Node r) {
		if(r == null)
			return new HelperForlengthOfLongestPath(0, -1);
	
		HelperForlengthOfLongestPath leftLength = lengthOfLongestPathBetweenAnyTwoNodes(r.left);
		HelperForlengthOfLongestPath rightLength = lengthOfLongestPathBetweenAnyTwoNodes(r.right);
		
		int longestPath = Math.max(Math.max(leftLength.longestPath, rightLength.longestPath), leftLength.height + rightLength.height +2);
		int height = Math.max(leftLength.height, rightLength.height) + 1;
		
//		if(r.left!=null) possibleLongest++;
//		if(r.right!=null) possibleLongest++;
//		if(r.left!=null || r.right!=null)
//			height++;
		
		System.out.println(r.data + ": " + longestPath + ", " + height);
		return new HelperForlengthOfLongestPath(longestPath, height);
	}

	static int ansLeftChildrenSum = 0;
	
	public int leftChildrenSum(Node r, Integer k) {
		if(r == null)
			return 0;
 		int leftAns = leftChildrenSum(r.left, k);
		leftChildrenSum(r.right, k);
		
		if(leftAns > k)
			ansLeftChildrenSum++;
		
		return leftAns + r.data;
	}
	
	public void leftChildrenSum(int k) {
		leftChildrenSum(this.root, k);
		System.out.println(ansLeftChildrenSum);
	}
	
}
