package tree;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		BinarySearchTree tree = new BinarySearchTree();
		
//		tree.insertIteratively(50);
//		tree.insertIteratively(20);
//		tree.insertIteratively(90);
//		tree.insertIteratively(85);
//		tree.insertIteratively(10);
//		tree.insertIteratively(45);
//		tree.insertIteratively(30);
//		tree.insertIteratively(100);
//		tree.insertIteratively(15);
//		tree.insertIteratively(75);
//		tree.insertIteratively(95);
//		tree.insertIteratively(120);
//		tree.insertIteratively(5);
//		tree.insertIteratively(50);
//		
//		tree.inOrderTraversal();
		System.out.println("===========================");
		
//		tree.insertRecursively(50);
//		tree.insertRecursively(20);
//		tree.insertRecursively(90);
//		tree.insertRecursively(85);
//		tree.insertRecursively(10);
//		tree.insertRecursively(45);
//		tree.insertRecursively(30);
//		tree.insertRecursively(100);
//		tree.insertRecursively(15);
//		tree.insertRecursively(75);
//		tree.insertRecursively(95);
//		tree.insertRecursively(120);
//		tree.insertRecursively(5);
//		tree.insertRecursively(50);
		
		tree.insertRecursively2(50);
		tree.insertRecursively2(20);
		tree.insertRecursively2(90);
		tree.insertRecursively2(85);
		tree.insertRecursively2(10);
		tree.insertRecursively2(45);
		tree.insertRecursively2(30);
		tree.insertRecursively2(100);
		tree.insertRecursively2(15);
		tree.insertRecursively2(75);
		tree.insertRecursively2(95);
		tree.insertRecursively2(120);
		tree.insertRecursively2(5);
		
		tree.inOrderTraversal();
		System.out.println("============================");
		
		tree.BFSTraversal();
		System.out.println("============================");
		tree.levelOrderLevelWise();
//		tree.deleteNode(50);
//		tree.inOrderTraversal();
		System.out.println("============================");
		tree.levelOrderLevelWise();
		System.out.println("============================");
		tree.topViewOfBinaryTree();
		System.out.println("============================");
		tree.bottomViewOfBinaryTree();
		System.out.println("============================");
		tree.isBST();
		System.out.println("============================");
		tree.isBalanced();
		System.out.println("============================");
		tree.lengthOfLongestPathBetweenAnyTwoNodes();
		System.out.println("============================");
		tree.heightOfTree();
		System.out.println("============================");
		tree.leftChildrenSum(50);
	}

}
