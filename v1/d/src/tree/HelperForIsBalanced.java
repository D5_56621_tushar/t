package tree;

class HelperForIsBalanced {
	boolean isBalanced;
	int height;
	
	public HelperForIsBalanced(boolean isBalanced, int height) {
		this.isBalanced = isBalanced;
		this.height = height;
	}

	@Override
	public String toString() {
		return "HelperForIsBalanced [isBalanced=" + isBalanced + ", height=" + height + "]";
	}
	
	
}

