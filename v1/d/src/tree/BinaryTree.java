//convertBTIntoBST keeping structure intact

package tree;

import java.util.ArrayList;

public class BinaryTree {

	private void addInArray(Node r, ArrayList<Integer> arr) {
		if(r == null)
			return;
		
		if(r.left!=null)
			addInArray(r.left, arr);
		arr.add(r.data);
		if(r.right!=null)
			addInArray(r.right, arr);
	}
	
	private void addInTree(Node r, ArrayList<Integer> arr, Integer i) {
		if(r == null)
			return;
		
		if(r.left!=null)
			addInTree(r.left, arr, i);
		r.data = arr.get(i); 
		i++;
		if(r.right!=null)
			addInTree(r.right, arr, i);
	}
	
	public void convertBTIntoBST() {
		if(root == null)
			return;
		
		ArrayList<Integer> arr = new ArrayList<Integer>();		
		addInArray(root, arr);
		arr.sort(null);
		
		addInTree(root, arr, 0);
	}
}
