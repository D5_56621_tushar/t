package tree;

class HelperForlengthOfLongestPath {
	int longestPath;
	int height;
	
	public HelperForlengthOfLongestPath(int longestPath, int height) {
		this.longestPath = longestPath;
		this.height = height;
	}

	@Override
	public String toString() {
		return "HelperForlengthOfLongestPath [longestPath=" + longestPath + ", height=" + height + "]";
	}
	
}
