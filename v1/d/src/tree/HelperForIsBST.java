package tree;

class HelperForIsBST {
	boolean isBST;
	int maxOfTree;
	int minOfTree;
	
	public HelperForIsBST(boolean isBST, int minOfTree, int maxOfTree) {
		this.isBST = isBST;
		this.minOfTree = minOfTree;
		this.maxOfTree = maxOfTree;
	}

	@Override
	public String toString() {
		return "HelperForIsBST [isBST=" + isBST + ", maxOfTree=" + maxOfTree + ", minOfTree=" + minOfTree + "]";
	}
	
	
}