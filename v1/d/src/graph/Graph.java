//displayGraph
//isTherePath
//displayAllPaths

package graph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;


public class Graph {

	int vertexCount;
	int edgeCount;
	ArrayList<ArrayList<Integer>> graph;
	
	public Graph(int vertexCount, int edgeCount) {
		graph = new ArrayList<ArrayList<Integer>>(vertexCount);
		this.vertexCount = vertexCount;
		this.edgeCount = edgeCount;
		
		for(int i=0; i<vertexCount; i++)
			graph.add(i, new ArrayList<Integer>());
	}
	
	public void acceptGraph() {
		Scanner sc = new Scanner(System.in);
		int n1, n2;
		
		for(int j = 0; j< this.edgeCount; j++) {
			System.out.println("Enter pair of nodes for edge: ");
			n1 = sc.nextInt();
			n2 = sc.nextInt();
			graph.get(n1).add(n2);
			graph.get(n2).add(n1);
		}
	}
	
	public void displayGraph() {
		int i=0;
		for (ArrayList<Integer> list : graph) {
			System.out.print(i + "====> ");
			list.forEach((e)->System.out.print(e + " "));
			System.out.println();
			i++;
		}
	}
	
	public boolean isTherePath(int from, int to, boolean[] visited) {
		visited[from] = true;
		if(from == to)
			return true;
		
		for (Integer neighbour : graph.get(from)) {
			if(visited[neighbour]!=true && isTherePath(neighbour, to, visited))
				return true;
		}
		
		return false;
	}
	
	public boolean isTherePath(int from, int to) { //direct or indirect
		boolean[] visited = new boolean[vertexCount];
		return isTherePath(from, to, visited);
	}
	
	public void displayAllPaths(int from , int to, boolean[] visited, int[] arr, Integer k) { //using array
		if(from == to) {
			for (int i=0; i<=k;i++) {
				System.out.print(arr[i] + " -> ");
			}
			System.out.println();
			return;
		}
		visited[from] = true;
		
		for (int neighbour : graph.get(from)) {
			k++;
			arr[k] = neighbour;
			if(visited[neighbour]!=true)
				displayAllPaths(neighbour, to,visited, arr, k);
			k--;
		}
		visited[from] = false;
	}
	
	public void displayAllPaths2(int from , int to, boolean[] visited, Stack<Integer> path) { //using stack
		if(from == to) {
			path.forEach((e)->System.out.print(e + " -> "));
			System.out.println();
			return;
		}
		visited[from] = true;
		
		for (int neighbour : graph.get(from)) {
			path.push(neighbour);
			if(visited[neighbour]!=true)
				displayAllPaths2(neighbour, to,visited, path);
			path.pop();
		}
		visited[from] = false;
	}
	
	public void displayAllPaths(int from, int to) {
		boolean[] visited = new boolean[vertexCount];
		int[] arr = new int[vertexCount];
		Stack<Integer> path = new Stack<Integer>();
		arr[0] = from;
		path.push(from);
		displayAllPaths(from, to, visited, arr, 0);
		System.out.println("===========================================================");
		displayAllPaths2(from, to, visited, path);
	}
	
	public void bfsTraversal(int start) {
		boolean[] visited = new boolean[vertexCount];
		int[] dist = new int[vertexCount];
		dist[start] = 0;
		Queue<Integer> q = new LinkedList<Integer>();
		q.add(start);
		visited[start] = true;
		int temp;
		
		while(!q.isEmpty()) {
			temp = q.remove();
			System.out.print(temp + " ");
			for (Integer neighbour : graph.get(temp)) {
				if(!visited[neighbour]) {
					visited[neighbour] = true;
					q.add(neighbour);
					dist[neighbour] = dist[temp] + 1;
				}
			}
		}
		System.out.println();
		for(int i=0; i<vertexCount; i++) {
			System.out.println(i + ": " + dist[i]);
		}
	}
}
