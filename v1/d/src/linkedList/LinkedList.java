package linkedList;

import linkedList.Node;

public class LinkedList {

	private Node head;
	private int nodeCount;
	
	public LinkedList() {
		this.head = null;
		this.nodeCount = 0;
	}
	
	public void addStart(int data) {
		Node newNode = new Node(data);
		if(head==null) {
			head = newNode;
			return;
		}
		Node temp = head;
		head=newNode;
		newNode.next = temp;
	}
	
	public void addLast(int data) {
		Node newNode = new Node(data);
		Node temp = head;
		while(temp.next!=null) {
			temp = temp.next;
		}
		temp.next = newNode;
	}
	
	
	void reverseTheListIteratively() {
		Node curr = head;
		Node prev = null, succ = head.next;
		
		while(curr.next!=null) {
			curr.next = prev;
			prev = curr;
			curr = succ;
			succ =succ.next;
		}
		curr.next = prev;
		this.head = curr;
	}
	
	void reverseTheListRecursively() {
		Node temp = head;
		reverseTheListRecursively(head, head.next);
		temp.next=null;
	}
	
	void reverseTheListRecursively(Node prev, Node curr) {
		if(curr.next==null) {
			this.head = curr;
			curr.next = prev;
			return;
		}
		reverseTheListRecursively(curr, curr.next);
		curr.next=prev;
	}
	
	void displayListInReverseOrder(Node temp) {
		if(temp==null)
			return;
		displayListInReverseOrder(temp.next);
		System.out.print(temp.data+" ");
	}
	
	void displayListInReverseOrder() {
		displayListInReverseOrder(head);
		System.out.println();
	}
	
	public void display() {
		if(head==null)
			System.out.println("List is Empty!!!");
		else {
			Node temp = head;
			while(temp!=null) {
				System.out.print(temp.data+" -> ");
				temp = temp.next;
			}
			System.out.println();
		}
//			System.out.println("LinkedList is: "+ str);
	}
	

	public Node reverseInGroups(Node h, int k) {
		if(h == null)
			return null;
		
		Node c = h;
		Node n = null;
		Node p = null;
		int count =0;
		
		while(c!=null && count<k) {
			n = c.next;
			c.next = p;
			p = c;
			c = n;
			count++;
		}
		
		if(c!=null)
			h.next = reverseInGroups(c, k);
		
		return p;
	}
	
	public void reverseInGroups(int k) {
		this.head = reverseInGroups(this.head, k);
	}
	
//	public Node reverseInHalves(Node h) {
//		if(h == null)
//			return null;
//		
//		Node c = h;
//		Node n = null;
//		Node p = null;
//		Node fast = h;
//		
//		while(fast!=null && fast.next!=null) {
//			
//		}
//	}
	
	public Node reverse(Node h) {
		Node temp = h;
		Node prev = null;
		Node succ = null;
		
		while(temp!=null) {
			succ = temp.next;
			temp.next = prev;
			prev = temp;
			temp = succ;
		}
		
		return prev;
	}
	
	public void reverseInHalves() {
		
		Node slow = this.head;
		Node fast = this.head;
		
		while(fast.next.next!=null && fast.next.next.next!=null) {
			slow = slow.next;
			fast = fast.next.next;
		}
		
		Node temp = slow.next;
		slow.next = null;
		
		Node z = this.head;
		this.head = reverse(this.head);
		
		if(fast.next.next == null) { //even
			z.next = reverse(temp);
		}
		else {
			z.next = temp;
			temp.next = reverse(temp.next);
		}
	}
}
