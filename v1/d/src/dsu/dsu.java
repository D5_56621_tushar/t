package dsu;

public class dsu {

	private int size;
	private int[] parent;
	private int[] arr;
	private int[] rank;
	
	public dsu(int size) {
		this.size = size;
		arr = new int[size+1];
		parent = new int[size+1];
		rank = new int[size+1];
		for(int i = 1; i<=size; i++) {
			parent[i] = i;
			arr[i] = i;
		}
	}
	
	public int get(int data) {
		if(data == parent[data]) 
			return data;
		parent[data] = get(parent[data]); //path compression
		return parent[data];
	}
	
	public void unionByRank(int a, int b) {
		int pa = this.get(a);
		int pb = this.get(b);
		if(rank[pa] == rank[pb])
			rank[pa]++;
		if(rank[pa]>rank[pb])
			parent[pb] = pa;
		else
			parent[pa] = pb;
	}
}
