package sorting;

public class Sort {
	
	static int comparisons;
	static int iterations;
	
	public static void swap(int[] arr, int i, int j) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

	public static void bubbleSort(int[] arr) {
		comparisons = 0;
		iterations = 0;
		for (int i = 0; i < arr.length-1; i++) {
			iterations++;
			boolean flag = true;
			for (int j = i + 1; j < arr.length; j++) {
				comparisons++;
				if(arr[i]>arr[j]) {
					swap(arr, i, j);
					flag = false;
				}
			}
			if(flag==true) break;
		}
		System.out.println("No of comparisons= " + comparisons);
		System.out.println("No of iterations= " + iterations);
	}
	
	public static void selectionSort(int[] arr) {
		int minIndex;
		comparisons = 0;
		iterations = 0;
		for(int i=0; i<arr.length;i++) {
			iterations++;
			minIndex = i;
			for(int j=i+1; j<arr.length; j++) {
				comparisons++;
				if(arr[j]<arr[minIndex])
					minIndex = j; 
			}
			swap(arr, minIndex, i);
		}
		System.out.println("No of comparisons= " + comparisons);
		System.out.println("No of iterations= " + iterations);
	}
	
	public static void insertionSort(int[] arr) {
		comparisons = 0;
		iterations = 0;
		int key;
		for(int i=1;i<arr.length;i++) {
			iterations++;
			key = arr[i];
			int j;
			for(j=i-1;j>=0;j--) {
				comparisons++;
				if(key<arr[j])
					arr[j+1] = arr[j];
				else
					break;
			}
			arr[j+1] = key;
		}
		System.out.println("No of comparisons= " + comparisons);
		System.out.println("No of iterations= " + iterations);
	}
	
	public static int[] merge(int[] arr1, int[] arr2) {
		int i = 0, j=0, k=0;
		int[] arr3 = new int[arr1.length+arr2.length];
		while(i<arr1.length && j<arr2.length) {
			if(arr1[i]<arr2[j])
				arr3[k++] = arr1[i++];
			else
				arr3[k++] = arr2[j++];
		}
		while(i<arr1.length)
			arr3[k++] = arr1[i++];
		while(j<arr2.length)
			arr3[k++] = arr2[j++];
		display(arr3);
		return null;
	}
	
	public static void mergeTwoSortedArays(int[] arr, int lb, int mid, int ub) {
		int[] newArr = new int[ub-lb+1];
		int i=lb, j=mid+1, k=0;
		for(int z=lb;z<=ub;z++) {
			System.out.print(arr[z]+" ");
		}
		System.out.println();
		while(i<=mid && j<=ub) {
			if(arr[i]<=arr[j]) {
				newArr[k++] = arr[i++];
			}
			else {
				newArr[k++] = arr[j++];
			}
		}
		while(i<=mid) newArr[k++] = arr[i++];
		while(j<=ub) newArr[k++] = arr[j++];
		
		for(i=0,j=lb; i<newArr.length && j<=ub; i++, j++)
			arr[j]=newArr[i];
	}
	
	public static void mergeSortHelper(int[] arr, int lb, int ub) {
		if(lb >= ub) return;
		int mid = lb + (ub-lb)/2;
		System.out.println("lb= "+lb+" mid= "+mid+" ub= "+ub);
		
		mergeSortHelper(arr, lb, mid);
		mergeSortHelper(arr, mid+1, ub);
		mergeTwoSortedArays(arr, lb, mid, ub);
		display(arr);
	}
	
	public static void mergeSort(int[] arr) {
		mergeSortHelper(arr, 0, arr.length-1);
	}
	
	public static void quickSort(int[] arr, int i, int j) {
		if(i>=j)
			return;
		
		int pivot = i;
		int p = i+1;
		int q = j;
		
		while(p<q) {
			while(p<j && p<arr.length && arr[p]<=arr[pivot]) {
				p++;
			}
			while(arr[q]>arr[pivot]) {
				q--;
			}
			if(p<q)
				swap(arr, p, q);
		}
		
		swap(arr, pivot, q);
		Sort.display(arr);
		quickSort(arr, pivot, q-1);
		quickSort(arr, q+1, j);
		
	}
	
	public static void quickSort(int[] arr) {
		quickSort(arr, 0, arr.length-1);
	}
	
	public static void display(int[] arr) {
		System.out.print("Sorted Array: ");
		for(int i=0;i<arr.length;i++) 
			System.out.print(arr[i]+" ");
		System.out.println();
	}
}
