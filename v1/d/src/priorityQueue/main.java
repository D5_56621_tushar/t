package priorityQueue;

import java.util.ArrayList;

public class main {
	
	public static void main(String[] args ) {
	
		PriorityQueue pq = new PriorityQueue();
		
		pq.displayPriorityQueue();
		pq.insert(5);
		pq.insert(25);
		pq.insert(10);
		pq.insert(35);
		pq.insert(20);
		pq.insert(95);
		pq.insert(70);
		pq.insert(65);
		pq.insert(50);
		pq.displayPriorityQueue();
		pq.removePeak();
		pq.displayPriorityQueue();
		pq.remove(1);
		pq.displayPriorityQueue();
		ArrayList<Integer> arr = new ArrayList<Integer>();
		arr.add(5);
		arr.add(25);
		arr.add(10);
		arr.add(35);
		arr.add(20);
		arr.add(95);
		arr.add(70);
		arr.add(65);
		arr.add(50);
		pq.buildHeap(arr);
		
	}
}