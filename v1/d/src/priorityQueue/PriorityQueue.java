//upHeapify
//downHeapify
//insert
//get
//removePeak
//remove
//displayPriorityQueue
//buildQueue (from an given array in O(n)) (general case would be O(n(log n)))

package priorityQueue;

import java.util.ArrayList;

public class PriorityQueue {
	
	
	//max heap
	ArrayList<Integer> heap;
	
	public PriorityQueue() {
		this.heap = new ArrayList<Integer>();
	}
	
	int temp;
	
	public void upHeapify(int idx) {
		if(idx == 0)
			return;
		
		int parent = (idx-1)/2;
		if(heap.get(parent)<heap.get(idx)) {
			temp = heap.get(parent);
			heap.set(parent, heap.get(idx));
			heap.set(idx, temp);
			upHeapify(parent);
		}
	}
	
	public void downHeapify(int idx) {
		if((2*idx + 2) >= heap.size())
			return;
		
		int leftChildIdx = 2*idx + 1;
		int rightChildIdx = 2*idx + 2;
		int largestIdx = idx;
		if(leftChildIdx<heap.size() && heap.get(idx)<heap.get(leftChildIdx))
			largestIdx = leftChildIdx;
		if(rightChildIdx<heap.size() && heap.get(largestIdx)<heap.get(rightChildIdx))
			largestIdx = rightChildIdx;
		if(largestIdx == idx)
			return;
		
		temp = heap.get(largestIdx);
		heap.set(largestIdx, heap.get(idx));
		heap.set(idx, temp);
		downHeapify(largestIdx);
	}
	
	public void insert(int value) {
		heap.add(value);
		upHeapify(heap.size() - 1);
	}
	
	public int get() {
		return heap.get(0);
	}
	
	public int removePeak() {
		int ans = heap.get(0);
		heap.set(0, heap.get(heap.size()-1));
		heap.remove(heap.size() - 1);
		downHeapify(0);
		return ans;
	}
	
	public void remove(int idx) {
		if(idx == 0) {
			this.removePeak();
			return;
		}
		
		if(idx == heap.size()-1)
			heap.remove(heap.size() -1);
		
		heap.set(idx, Integer.MAX_VALUE);
		this.upHeapify(idx);
		this.removePeak();
	}
	
	public void displayPriorityQueue() {
		for (Integer i : heap)
			System.out.print(i + " ");
		System.out.println("================================================");
	}
	
	public void buildHeap(ArrayList<Integer> arr) {
		ArrayList<Integer> temp = this.heap;
		this.heap = arr;
		for(int i = heap.size()-1; i>=0; i--) 
			this.downHeapify(i);
		
		for (Integer i : heap)
			System.out.print(i + " ");
		System.out.println("================================================");
		
		this.heap = arr;
	}
}
